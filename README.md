# terreta 

> personal page for albertabril.com

![landscape](https://bytebucket.org/aabril/terreta/raw/master/src/home/static/images/albufera-valencia1-750x400.jpg)

terreta is a fork of [terra](http://github.com/aabril/terra)
the goal is to deploy my own private version of sputnik in terreta to albertabril.com
later I can add changes to the public sputnik boilerplate
Originally built with NuxtJS + ExpressJS + statics

## Exposed URLS

```bash
# nuxt
/                   : main
/*                  : 404

# statics
/slides             : 
/slides/example     : 

# api
/api/users          : list of users

```

## Build Setup

``` bash
# install dependencies
$ npm install # Or yarn install

# serve with hot reload at localhost:3000
$ npm run dev

# build for production and launch server
$ npm start
```

For detailed explanation on how things work, checkout the [Nuxt.js docs](https://github.com/nuxt/nuxt.js).

## Backpack

We use [backpack](https://github.com/palmerhq/backpack) to watch and build the application, so you can use the latest ES6 features (module syntax, async/await, etc.).
