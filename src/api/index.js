import { Router } from 'express'

import things from './resources/things'

const router = Router()

router.use('/things', things)

export default router
