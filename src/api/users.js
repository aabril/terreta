import { Router } from 'express'
import logger from '../server/logger'

const router = Router()

const users = [
  { name: 'Alexandre' },
  { name: 'Pooya' },
  { name: 'Sébastien' },
]

const getUsers = (req, res, next) => {
  logger.info('getUsers accessed')
  res.json(users)
}

const getUserById = (req, res, next) => {
  const id = parseInt(req.params.id)
  if (id >= 0 && id < users.length) {
    res.json(users[id])
  } else {
    res.sendStatus(404)
  }
}

router.get('/users', getUsers)
router.get('/users/:id', getUserById)

export default router
