export default {
    authenticate(plainText) {
      return this.encryptPassword(plainText) === this.hashedPassword;
    },
  
    makeSalt() {
      return crypto.randomBytes(16).toString('base64');
    },
  
    encryptPassword(password) {
      if (!password || !this.salt) return '';
      const salt = new Buffer(this.salt, 'base64');
      return crypto.pbkdf2Sync(password, salt, 10000, 64, 'sha512').toString('base64');
    }
}