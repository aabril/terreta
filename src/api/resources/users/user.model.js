import mongoose from 'mongoose'

const Schema = mongoose.Schema;
const schemaObj = {
  email: {
    type: String,
    unique: true,
    required: true,
    trim: true
  },
  username: {
    type: String,
    unique: true,
    required: true,
    trim: true
  },
  password: {
    type: String,
    required: true,
  },
  passwordConf: {
    type: String,
    required: true,
  }
}
const ThingSchema = new Schema(schemaObj, {timestamps: true});

const toJsonConfig = {
  virtuals: false,
  transform: (doc, ret, options) => {
    ret.id = ret._id;
    delete ret._id;
    delete ret.__v;
  }
}
ThingSchema.set('toJSON', toJsonConfig);

export default mongoose.model('Thing', ThingSchema)