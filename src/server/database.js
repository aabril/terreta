import mongoose from 'mongoose'
import logger from "./logger"

export default () => {
  const MONGO_URI = process.env.MONGO_URI;
  mongoose
   .connect(MONGO_URI)
   .then(() =>  logger.info('database connection succesful'))
   .catch((err) => logger.error(err))
}