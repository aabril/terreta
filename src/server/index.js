require('dotenv').config()

import express from 'express'
import bodyParser from 'body-parser'
import { Nuxt, Builder } from 'nuxt'
import routes from './routes'
import logger from './logger'
import database from './database'

process.on('unhandledRejection', (reason, p) => {
  return console.error('Unhandled Rejection at: Promise ', p, reason);
  process.exit()
});

function initialise() {
  const app = express()
  const host = process.env.HOST || '127.0.0.1'
  const port = process.env.PORT || 3000
  
  app.set('port', port)
  app.use(bodyParser.json());
  app.use(bodyParser.urlencoded({ extended: false }));
  
  database()

  // Import and Set Nuxt.js options
  let config = require('../../nuxt.config.js')
  config.dev = !(process.env.NODE_ENV === 'production')
  
  // Init Nuxt.js
  const nuxt = new Nuxt(config)
  
  // Build only in dev mode
  if (config.dev) {
    const builder = new Builder(nuxt)
    builder.build()
  }

  app.use(routes)

  // Give nuxt middleware to express
  app.use(nuxt.render)

  
  // Listen the server
  app.listen(port, host)
  logger.info('Server listening on ' + host + ':' + port) // eslint-disable-line no-console
}

export { initialise }