import Pino from 'pino'
const pino = Pino()


const fs = require('fs')
const pinoms = require('pino-multi-stream')
const streams = [
  {stream: fs.createWriteStream('/tmp/terreta.info.stream.out')},
  {level: 'fatal', stream: fs.createWriteStream('/tmp/terreta.fatal.stream.out')}
]
const log = pinoms({streams: streams})

function info(...args){
  log.info(...args)
}

function error(...args){
  log.error(...args)
}

export default { info, error }